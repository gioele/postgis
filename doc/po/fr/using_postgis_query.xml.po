# SOME DESCRIPTIVE TITLE.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-07-15 01:29+0000\n"
"PO-Revision-Date: 2023-05-12 10:08+0000\n"
"Last-Translator: Vincent Bre <vincent.bre@oslandia.com>\n"
"Language-Team: French <https://weblate.osgeo.org/projects/postgis/"
"using_postgis_queryxml/fr/>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 4.16.4\n"

#. Tag: title
#: using_postgis_query.xml:3
#, no-c-format
msgid "Spatial Queries"
msgstr "Requêtes spatiales"

#. Tag: para
#: using_postgis_query.xml:5
#, no-c-format
msgid ""
"The <emphasis>raison d'etre</emphasis> of spatial databases is to perform "
"queries inside the database which would ordinarily require desktop GIS "
"functionality. Using PostGIS effectively requires knowing what spatial "
"functions are available, how to use them in queries, and ensuring that "
"appropriate indexes are in place to provide good performance."
msgstr ""
"La <emphasis>raison d'être</emphasis> des bases de données spatiales est de "
"réaliser à l'intérieur de la base de données des requêtes qui normalement "
"nécessiteraient des fonctionnalités d'un SIG Desktop. Utiliser PostGIS "
"requiert en effet la connaissance de quelles fonctions spatiales sont "
"disponibles, comment les utiliser dans une requête, et de s'assurer de la "
"mise en place adéquate des index, pour une bonne performance."

#. Tag: title
#: using_postgis_query.xml:14
#, no-c-format
msgid "Determining Spatial Relationships"
msgstr "Déterminer les relations spatiales"

#. Tag: para
#: using_postgis_query.xml:16
#, no-c-format
msgid ""
"Spatial relationships indicate how two geometries interact with one another. "
"They are a fundamental capability for querying geometry."
msgstr ""
"Les relations spatiales indiquent comment deux géométries interagissent "
"l'une avec l'autre. Elles constituent une fonctionnalité fondamentale pour "
"effectuer des requêtes sur des géométries."

#. Tag: title
#: using_postgis_query.xml:21
#, no-c-format
msgid "Dimensionally Extended 9-Intersection Model"
msgstr "Modèle à 9 intersections dimensionnellement étendu"

#. Tag: para
#: using_postgis_query.xml:23
#, no-c-format
msgid ""
"According to the <ulink url=\"http://www.opengeospatial.org/standards/"
"sfs\">OpenGIS Simple Features Implementation Specification for SQL</ulink>, "
"\"the basic approach to comparing two geometries is to make pair-wise tests "
"of the intersections between the Interiors, Boundaries and Exteriors of the "
"two geometries and to classify the relationship between the two geometries "
"based on the entries in the resulting 'intersection' matrix.\""
msgstr ""
"Selon le <ulink url=\"http://www.opengeospatial.org/standards/sfs\">OpenGIS "
"Simple Features Implementation Specification for SQL</ulink>, \"l'approche "
"de base pour comparer deux géométries consiste à effectuer des tests par "
"paire des intersections entre les intérieurs, les limites et les extérieurs "
"des deux géométries et à classer la relation entre les deux géométries sur "
"la base des entrées de la matrice \"intersection\" résultante\"."

#. Tag: para
#: using_postgis_query.xml:32
#, no-c-format
msgid ""
"In the theory of point-set topology, the points in a geometry embedded in 2-"
"dimensional space are categorized into three sets:"
msgstr ""
"Dans la théorie de la topologie des ensembles de points, les points d'une "
"géométrie intégrée dans un espace à deux dimensions sont classés en trois "
"ensembles :"

#. Tag: glossterm
#: using_postgis_query.xml:38
#, no-c-format
msgid "<glossterm>Boundary</glossterm>"
msgstr "<glossterm>Limites</glossterm>"

#. Tag: para
#: using_postgis_query.xml:41
#, no-c-format
msgid ""
"The boundary of a geometry is the set of geometries of the next lower "
"dimension. For <varname>POINT</varname>s, which have a dimension of 0, the "
"boundary is the empty set. The boundary of a <varname>LINESTRING</varname> "
"is the two endpoints. For <varname>POLYGON</varname>s, the boundary is the "
"linework of the exterior and interior rings."
msgstr ""
"La limite d'une géométrie est l'ensemble des géométries de la dimension "
"immédiatement inférieure. Pour les <varname>POINT</varname>, qui ont une "
"dimension de 0, la limite est l'ensemble vide. La limite d'une "
"<varname>LINESTRING</varname> est constituée par ses deux extrémités. Pour "
"les <varname>POLYGON</varname>, la limite est le réseau de lignes des "
"anneaux extérieur et intérieur."

#. Tag: glossterm
#: using_postgis_query.xml:52
#, no-c-format
msgid "<glossterm>Interior</glossterm>"
msgstr "<glossterm>Intérieur</glossterm>"

#. Tag: para
#: using_postgis_query.xml:55
#, no-c-format
msgid ""
"The interior of a geometry are those points of a geometry that are not in "
"the boundary. For <varname>POINT</varname>s, the interior is the point "
"itself. The interior of a <varname>LINESTRING</varname> is the set of points "
"between the endpoints. For <varname>POLYGON</varname>s, the interior is the "
"areal surface inside the polygon."
msgstr ""
"L'intérieur d'une géométrie est constitué des points de la géométrie qui ne "
"se trouvent pas dans la limite. Pour les <varname>POINT</varname>, "
"l'intérieur est le point lui-même. L'intérieur d'une <varname>LINESTRING</"
"varname> est l'ensemble des points situés entre les extrémités. Pour les "
"<varname>POLYGON</varname>, l'intérieur est la surface aréale à l'intérieur "
"du polygone."

#. Tag: glossterm
#: using_postgis_query.xml:66
#, no-c-format
msgid "<glossterm>Exterior</glossterm>"
msgstr "<glossterm>Extérieur</glossterm>"

#. Tag: para
#: using_postgis_query.xml:69
#, no-c-format
msgid ""
"The exterior of a geometry is the rest of the space in which the geometry is "
"embedded; in other words, all points not in the interior or on the boundary "
"of the geometry. It is a 2-dimensional non-closed surface."
msgstr ""
"L'extérieur d'une géométrie est le reste de l'espace dans lequel la "
"géométrie est intégrée ; en d'autres termes, tous les points qui ne se "
"trouvent pas à l'intérieur ou sur la limite de la géométrie. Il s'agit d'une "
"surface non fermée à 2 dimensions."

#. Tag: para
#: using_postgis_query.xml:78
#, no-c-format
msgid ""
"The <ulink url=\"http://en.wikipedia.org/wiki/DE-9IM\">Dimensionally "
"Extended 9-Intersection Model</ulink> (DE-9IM) describes the spatial "
"relationship between two geometries by specifying the dimensions of the 9 "
"intersections between the above sets for each geometry. The intersection "
"dimensions can be formally represented in a 3x3 <emphasis "
"role=\"bold\">intersection matrix</emphasis>."
msgstr ""
"Le <ulink url=\"http://en.wikipedia.org/wiki/DE-9IM\">Dimensionally Extended "
"9-Intersection Model</ulink> (DE-9IM) décrit la relation spatiale entre deux "
"géométries en spécifiant les dimensions des 9 intersections entre les "
"ensembles ci-dessus pour chaque géométrie. Les dimensions des intersections "
"peuvent être représentées formellement dans une <emphasis "
"role=\"bold\">matrice d'intersections</emphasis> de 3x3."

#. Tag: para
#: using_postgis_query.xml:85
#, no-c-format
msgid ""
"For a geometry <emphasis>g</emphasis> the <emphasis>Interior</emphasis>, "
"<emphasis>Boundary</emphasis>, and <emphasis>Exterior</emphasis> are denoted "
"using the notation <emphasis>I(g)</emphasis>, <emphasis>B(g)</emphasis>, and "
"<emphasis>E(g)</emphasis>. Also, <emphasis>dim(s)</emphasis> denotes the "
"dimension of a set <emphasis>s</emphasis> with the domain of <literal>{0,1,2,"
"F}</literal>:"
msgstr ""
"Pour une géométrie <emphasis>g</emphasis>, les <emphasis>Limites</emphasis>, "
"<emphasis>Intérieures</emphasis>, et <emphasis>Extérieures</emphasis> sont "
"désignés par la notation <emphasis>I(g)</emphasis>, <emphasis>B(g)</"
"emphasis>, et <emphasis>E(g)</emphasis>. En outre, <emphasis>dim(s)</"
"emphasis> désigne la dimension d'un ensemble <emphasis>s</emphasis> dont le "
"domaine est <literal>{0,1,2,F}</literal> :"

#. Tag: para
#: using_postgis_query.xml:97
#, no-c-format
msgid "<literal>0</literal> =&gt; point"
msgstr "<literal>0</literal> =&gt; point"

#. Tag: para
#: using_postgis_query.xml:101
#, no-c-format
msgid "<literal>1</literal> =&gt; line"
msgstr "<literal>1</literal> =&gt; ligne"

#. Tag: para
#: using_postgis_query.xml:105
#, no-c-format
msgid "<literal>2</literal> =&gt; area"
msgstr "<literal>2</literal> =&gt; area"

#. Tag: para
#: using_postgis_query.xml:109
#, no-c-format
msgid "<literal>F</literal> =&gt; empty set"
msgstr "<literal>F</literal> =&gt; ensemble vide"

#. Tag: para
#: using_postgis_query.xml:114
#, no-c-format
msgid ""
"Using this notation, the intersection matrix for two geometries <emphasis>a</"
"emphasis> and <emphasis>b</emphasis> is:"
msgstr ""
"En utilisant cette notation, la matrice d'intersection pour deux géométries "
"<emphasis>a</emphasis> et <emphasis>b</emphasis> est :"

#. Tag: emphasis
#: using_postgis_query.xml:123 using_postgis_query.xml:131
#: using_postgis_query.xml:182 using_postgis_query.xml:195
#, no-c-format
msgid "<emphasis role=\"bold\">Interior</emphasis>"
msgstr "<emphasis role=\"bold\">Intérieur</emphasis>"

#. Tag: emphasis
#: using_postgis_query.xml:124 using_postgis_query.xml:137
#: using_postgis_query.xml:185 using_postgis_query.xml:218
#, no-c-format
msgid "<emphasis role=\"bold\">Boundary</emphasis>"
msgstr "<emphasis role=\"bold\">Limite</emphasis>"

#. Tag: emphasis
#: using_postgis_query.xml:125 using_postgis_query.xml:143
#: using_postgis_query.xml:188 using_postgis_query.xml:241
#, no-c-format
msgid "<emphasis role=\"bold\">Exterior</emphasis>"
msgstr "<emphasis role=\"bold\">Extérieur</emphasis>"

#. Tag: emphasis
#: using_postgis_query.xml:132
#, no-c-format
msgid "dim( I(a) ∩ I(b) )"
msgstr "dim( I(a) ∩ I(b) )"

#. Tag: emphasis
#: using_postgis_query.xml:133
#, no-c-format
msgid "dim( I(a) ∩ B(b) )"
msgstr "dim( I(a) ∩ B(b) )"

#. Tag: emphasis
#: using_postgis_query.xml:134
#, no-c-format
msgid "dim( I(a) ∩ E(b) )"
msgstr "dim( I(a) ∩ E(b) )"

#. Tag: emphasis
#: using_postgis_query.xml:138
#, no-c-format
msgid "dim( B(a) ∩ I(b) )"
msgstr "dim( B(a) ∩ I(b) )"

#. Tag: emphasis
#: using_postgis_query.xml:139
#, no-c-format
msgid "dim( B(a) ∩ B(b) )"
msgstr "dim( B(a) ∩ B(b) )"

#. Tag: emphasis
#: using_postgis_query.xml:140
#, no-c-format
msgid "dim( B(a) ∩ E(b) )"
msgstr "dim( B(a) ∩ E(b) )"

#. Tag: emphasis
#: using_postgis_query.xml:144
#, no-c-format
msgid "dim( E(a) ∩ I(b) )"
msgstr "dim( E(a) ∩ I(b) )"

#. Tag: emphasis
#: using_postgis_query.xml:145
#, no-c-format
msgid "dim( E(a) ∩ B(b) )"
msgstr "dim( E(a) ∩ B(b) )"

#. Tag: emphasis
#: using_postgis_query.xml:146
#, no-c-format
msgid "dim( E(a) ∩ E(b) )"
msgstr "dim( E(a) ∩ E(b) )"

#. Tag: para
#: using_postgis_query.xml:153
#, no-c-format
msgid "Visually, for two overlapping polygonal geometries, this looks like:"
msgstr ""
"Visuellement, pour deux géométries polygonales qui se chevauchent, cela "
"ressemble à ce qui suit :"

#. Tag: para
#: using_postgis_query.xml:199
#, no-c-format
msgid ""
"<emphasis>dim( I(a) ∩ I(b) ) = </emphasis><emphasis role=\"bold\">2</"
"emphasis>"
msgstr ""
"<emphasis>dim( I(a) ∩ I(b) ) = </emphasis><emphasis role=\"bold\">2</"
"emphasis>"

#. Tag: para
#: using_postgis_query.xml:205
#, no-c-format
msgid ""
"<emphasis>dim( I(a) ∩ B(b) = </emphasis><emphasis role=\"bold\">1</emphasis>"
msgstr ""
"<emphasis>dim( I(a) ∩ B(b) = </emphasis><emphasis role=\"bold\">1</emphasis>"

#. Tag: para
#: using_postgis_query.xml:211
#, no-c-format
msgid ""
"<emphasis>dim( I(a) ∩ E(b) ) = </emphasis><emphasis role=\"bold\">2</"
"emphasis>"
msgstr ""
"<emphasis>dim( I(a) ∩ E(b) ) = </emphasis><emphasis role=\"bold\">2</"
"emphasis>"

#. Tag: para
#: using_postgis_query.xml:222
#, no-c-format
msgid ""
"<emphasis>dim( B(a) ∩ I(b) ) = </emphasis><emphasis role=\"bold\">1</"
"emphasis>"
msgstr ""
"<emphasis>dim( B(a) ∩ I(b) ) = </emphasis><emphasis role=\"bold\">1</"
"emphasis>"

#. Tag: para
#: using_postgis_query.xml:228
#, no-c-format
msgid ""
"<emphasis>dim( B(a) ∩ B(b) ) = </emphasis><emphasis role=\"bold\">0</"
"emphasis>"
msgstr ""
"<emphasis>dim( B(a) ∩ B(b) ) = </emphasis><emphasis role=\"bold\">0</"
"emphasis>"

#. Tag: para
#: using_postgis_query.xml:234
#, no-c-format
msgid ""
"<emphasis>dim( B(a) ∩ E(b) ) = </emphasis><emphasis role=\"bold\">1</"
"emphasis>"
msgstr ""
"<emphasis>dim( B(a) ∩ E(b) ) = </emphasis><emphasis role=\"bold\">1</"
"emphasis>"

#. Tag: para
#: using_postgis_query.xml:245
#, no-c-format
msgid ""
"<emphasis>dim( E(a) ∩ I(b) ) = </emphasis><emphasis role=\"bold\">2</"
"emphasis>"
msgstr ""
"<emphasis>dim( E(a) ∩ I(b) ) = </emphasis><emphasis role=\"bold\">2</"
"emphasis>"

#. Tag: para
#: using_postgis_query.xml:251
#, no-c-format
msgid ""
"<emphasis>dim( E(a) ∩ B(b) ) = </emphasis><emphasis role=\"bold\">1</"
"emphasis>"
msgstr ""
"<emphasis>dim( E(a) ∩ B(b) ) = </emphasis><emphasis role=\"bold\">1</"
"emphasis>"

#. Tag: para
#: using_postgis_query.xml:257
#, no-c-format
msgid ""
"<emphasis>dim( E(a) ∩ E(b) = </emphasis><emphasis role=\"bold\">2</emphasis>"
msgstr ""
"<emphasis>dim( E(a) ∩ E(b) = </emphasis><emphasis role=\"bold\">2</emphasis>"

#. Tag: para
#: using_postgis_query.xml:269
#, no-c-format
msgid ""
"Reading from left to right and top to bottom, the intersection matrix is "
"represented as the text string '<emphasis role=\"bold\">212101212</"
"emphasis>'."
msgstr ""
"En lisant de gauche à droite et de haut en bas, la matrice d'intersection "
"est représentée par la chaîne de texte \"<emphasis role=\"bold\">212101212</"
"emphasis>\"."

#. Tag: para
#: using_postgis_query.xml:272
#, no-c-format
msgid "For more information, refer to:"
msgstr "Pour plus d'informations, voir :"

#. Tag: para
#: using_postgis_query.xml:276
#, no-c-format
msgid ""
"<ulink url=\"http://www.opengeospatial.org/standards/sfs\">OpenGIS Simple "
"Features Implementation Specification for SQL</ulink> (version 1.1, section "
"2.1.13.2)"
msgstr ""
"<ulink url=\"http://www.opengeospatial.org/standards/sfs\">OpenGIS Simple "
"Features Implementation Specification for SQL</ulink> (version 1.1, section "
"2.1.13.2)"

#. Tag: ulink
#: using_postgis_query.xml:281
#, no-c-format
msgid "Wikipedia: Dimensionally Extended Nine-Intersection Model (DE-9IM)"
msgstr "Wikipedia : Dimensionally Extended Nine-Intersection Model (DE-9IM)"

#. Tag: ulink
#: using_postgis_query.xml:285
#, no-c-format
msgid "GeoTools: Point Set Theory and the DE-9IM Matrix"
msgstr "GeoTools : Théorie des ensembles de points et matrice DE-9IM"

#. Tag: title
#: using_postgis_query.xml:292
#, no-c-format
msgid "Named Spatial Relationships"
msgstr "Relations spatiales nommées"

#. Tag: para
#: using_postgis_query.xml:294
#, no-c-format
msgid ""
"To make it easy to determine common spatial relationships, the OGC SFS "
"defines a set of <emphasis>named spatial relationship predicates</emphasis>. "
"PostGIS provides these as the functions <xref linkend=\"ST_Contains\"/>, "
"<xref linkend=\"ST_Crosses\"/>, <xref linkend=\"ST_Disjoint\"/>, <xref "
"linkend=\"ST_Equals\"/>, <xref linkend=\"ST_Intersects\"/>, <xref "
"linkend=\"ST_Overlaps\"/>, <xref linkend=\"ST_Touches\"/>, <xref "
"linkend=\"ST_Within\"/>. It also defines the non-standard relationship "
"predicates <xref linkend=\"ST_Covers\"/>, <xref linkend=\"ST_CoveredBy\"/>, "
"and <xref linkend=\"ST_ContainsProperly\"/>."
msgstr ""
"Pour faciliter la détermination des relations spatiales communes, l'OGC SFS "
"définit un ensemble de <emphasis>prédicats de relations spatiales</"
"emphasis>. PostGIS fournit ces prédicats sous la forme des fonctions <xref "
"linkend=\"ST_Contains\"/>, <xref linkend=\"ST_Crosses\"/>, <xref "
"linkend=\"ST_Disjoint\"/>, <xref linkend=\"ST_Equals\"/>, <xref "
"linkend=\"ST_Intersects\"/>, <xref linkend=\"ST_Overlaps\"/>, <xref "
"linkend=\"ST_Touches\"/>, <xref linkend=\"ST_Within\"/>. Il définit "
"également les prédicats de relation non standard <xref linkend=\"ST_Covers\"/"
">, <xref linkend=\"ST_CoveredBy\"/>, et <xref "
"linkend=\"ST_ContainsProperly\"/>."

#. Tag: para
#: using_postgis_query.xml:305
#, no-c-format
msgid ""
"Spatial predicates are usually used as conditions in SQL <code>WHERE</code> "
"or <code>JOIN</code> clauses. The named spatial predicates automatically use "
"a spatial index if one is available, so there is no need to use the bounding "
"box operator <code>&amp;&amp;</code> as well. For example:"
msgstr ""
"Les prédicats spatiaux sont généralement utilisés comme conditions dans les "
"clauses SQL <code>WHERE</code> ou <code>JOIN</code>. Les prédicats spatiaux "
"nommés utilisent automatiquement un index spatial s'il existe, de sorte "
"qu'il n'est pas nécessaire d'utiliser également l'opérateur de boîte de "
"délimitation <code>&amp;&amp;</code>. Par exemple :"

#. Tag: para
#: using_postgis_query.xml:313
#, no-c-format
msgid ""
"For more details and illustrations, see the <ulink url=\"https://postgis.net/"
"workshops/postgis-intro/spatial_relationships.html\">PostGIS Workshop.</"
"ulink>"
msgstr ""
"Pour plus de détails et d'illustrations, voir l' <ulink url=\"https://"
"postgis.net/workshops/postgis-intro/spatial_relationships.html\">Atelier "
"PostGIS.</ulink>"

#. Tag: title
#: using_postgis_query.xml:319
#, no-c-format
msgid "General Spatial Relationships"
msgstr "Relations spatiales générales"

#. Tag: para
#: using_postgis_query.xml:321
#, no-c-format
msgid ""
"In some cases the named spatial relationships are insufficient to provide a "
"desired spatial filter condition."
msgstr ""
"Dans certains cas, les relations spatiales nommées sont insuffisantes pour "
"fournir une condition de filtrage spatial souhaitée."

#. Tag: para
#: using_postgis_query.xml:331
#, no-c-format
msgid ""
"For example, consider a linear dataset representing a road network. It may "
"be required to identify all road segments that cross each other, not at a "
"point, but in a line (perhaps to validate some business rule). In this case "
"<xref linkend=\"ST_Crosses\"/> does not provide the necessary spatial "
"filter, since for linear features it returns <varname>true</varname> only "
"where they cross at a point."
msgstr ""
"Prenons l'exemple d'un ensemble de données linéaires représentant un réseau "
"routier. Il peut être nécessaire d'identifier tous les segments de route qui "
"se croisent, non pas en un point, mais en une ligne (peut-être pour valider "
"une règle commerciale). Dans ce cas, <xref linkend=\"ST_Crosses\"/> ne "
"fournit pas le filtre spatial nécessaire, puisque pour les caractéristiques "
"linéaires, il renvoie <varname>true</varname> uniquement lorsqu'elles se "
"croisent en un point."

#. Tag: para
#: using_postgis_query.xml:339
#, no-c-format
msgid ""
"A two-step solution would be to first compute the actual intersection (<xref "
"linkend=\"ST_Intersection\"/>) of pairs of road lines that spatially "
"intersect (<xref linkend=\"ST_Intersects\"/>), and then check if the "
"intersection's <xref linkend=\"ST_GeometryType\"/> is '<varname>LINESTRING</"
"varname>' (properly dealing with cases that return "
"<varname>GEOMETRYCOLLECTION</varname>s of <varname>[MULTI]POINT</varname>s, "
"<varname>[MULTI]LINESTRING</varname>s, etc.)."
msgstr ""
"Une solution en deux étapes consisterait à calculer d'abord l'intersection "
"réelle (<xref linkend=\"ST_Intersection\"/>) des paires de lignes routières "
"qui se croisent dans l'espace (<xref linkend=\"ST_Intersects\"/>), puis "
"vérifier si le <xref linkend=\"ST_GeometryType\"/> de l'intersection est "
"\"<varname>LINESTRING</varname>\" (en traitant correctement les cas qui "
"renvoient des <varname>GEOMETRYCOLLECTION</varname>s de "
"<varname>[MULTI]POINT</varname>s, <varname>[MULTI]LINESTRING</varname>s, "
"etc. )."

#. Tag: para
#: using_postgis_query.xml:348
#, no-c-format
msgid "Clearly, a simpler and faster solution is desirable."
msgstr ""
"Il est évident qu'une solution plus simple et plus rapide est souhaitable."

#. Tag: para
#: using_postgis_query.xml:360
#, no-c-format
msgid ""
"A second example is locating wharves that intersect a lake's boundary on a "
"line and where one end of the wharf is up on shore. In other words, where a "
"wharf is within but not completely contained by a lake, intersects the "
"boundary of a lake on a line, and where exactly one of the wharf's endpoints "
"is within or on the boundary of the lake. It is possible to use a "
"combination of spatial predicates to find the required features:"
msgstr ""
"Un deuxième exemple est la localisation des quais qui coupent les limites "
"d'un lac sur une ligne et dont l'une des extrémités se trouve sur le rivage. "
"En d'autres termes, lorsqu'un quai est situé à l'intérieur d'un lac mais "
"n'est pas complètement contenu par celui-ci, qu'il coupe la limite d'un lac "
"sur une ligne et que l'une des extrémités du quai se trouve à l'intérieur ou "
"sur la limite du lac. Il est possible d'utiliser une combinaison de "
"prédicats spatiaux pour trouver les caractéristiques requises :"

#. Tag: para
#: using_postgis_query.xml:371
#, no-c-format
msgid "(lake, wharf) = TRUE"
msgstr "(lake, wharf) = TRUE"

#. Tag: para
#: using_postgis_query.xml:375
#, no-c-format
msgid "(lake, wharf) = FALSE"
msgstr "(lake, wharf) = FALSE"

#. Tag: para
#: using_postgis_query.xml:379
#, no-c-format
msgid "(<xref linkend=\"ST_Intersection\"/>(wharf, lake)) = 'LINESTRING'"
msgstr "(<xref linkend=\"ST_Intersection\"/>(wharf, lake)) = 'LINESTRING'"

#. Tag: para
#: using_postgis_query.xml:384
#, no-c-format
msgid ""
"(<xref linkend=\"ST_Multi\"/>(<xref linkend=\"ST_Intersection\"/>(<xref "
"linkend=\"ST_Boundary\"/>(wharf), <xref linkend=\"ST_Boundary\"/>(lake)))) = "
"1"
msgstr ""
"(<xref linkend=\"ST_Multi\"/>(<xref linkend=\"ST_Intersection\"/>(<xref "
"linkend=\"ST_Boundary\"/>(wharf), <xref linkend=\"ST_Boundary\"/>(lake)))) = "
"1"

#. Tag: para
#: using_postgis_query.xml:387
#, no-c-format
msgid "... but needless to say, this is quite complicated."
msgstr "... mais inutile de dire que c'est assez compliqué."

#. Tag: para
#: using_postgis_query.xml:395
#, no-c-format
msgid ""
"These requirements can be met by computing the full DE-9IM intersection "
"matrix. PostGIS provides the <xref linkend=\"ST_Relate\"/> function to do "
"this:"
msgstr ""
"Ces exigences peuvent être satisfaites en calculant la matrice "
"d'intersection DE-9IM complète. PostGIS fournit la fonction <xref "
"linkend=\"ST_Relate\"/> pour ce faire :"

#. Tag: para
#: using_postgis_query.xml:402
#, no-c-format
msgid ""
"To test a particular spatial relationship, an <emphasis "
"role=\"bold\">intersection matrix pattern</emphasis> is used. This is the "
"matrix representation augmented with the additional symbols <literal>{T,*}</"
"literal>:"
msgstr ""
"Pour tester une relation spatiale particulière, un <emphasis "
"role=\"bold\">modèle de matrice d'intersection</emphasis> est utilisé. Il "
"s'agit de la représentation matricielle augmentée des symboles "
"supplémentaires <literal>{T,*}</literal> :"

#. Tag: para
#: using_postgis_query.xml:410
#, no-c-format
msgid ""
"<literal>T</literal> =&gt; intersection dimension is non-empty; i.e. is in "
"<literal>{0,1,2}</literal>"
msgstr ""
"<literal>T</literal> =&gt; la dimension d'intersection est non vide ; c'est-"
"à-dire qu'elle se trouve dans <literal>{0,1,2}</literal>"

#. Tag: para
#: using_postgis_query.xml:415
#, no-c-format
msgid "<literal>*</literal> =&gt; don't care"
msgstr "<literal>*</literal> =&gt; indifférent"

#. Tag: para
#: using_postgis_query.xml:419
#, no-c-format
msgid ""
"Using intersection matrix patterns, specific spatial relationships can be "
"evaluated in a more succinct way. The <xref linkend=\"ST_Relate\"/> and the "
"<xref linkend=\"ST_RelateMatch\"/> functions can be used to test "
"intersection matrix patterns. For the first example above, the intersection "
"matrix pattern specifying two lines intersecting in a line is '<emphasis "
"role=\"bold\">1*1***1**</emphasis>':"
msgstr ""
"Les modèles de matrice d'intersection permettent d'évaluer des relations "
"spatiales spécifiques de manière plus succincte. Les fonctions <xref "
"linkend=\"ST_Relate\"/> et <xref linkend=\"ST_RelateMatch\"/> peuvent être "
"utilisées pour tester les modèles de matrice d'intersection. Pour le premier "
"exemple ci-dessus, le modèle de matrice d'intersection spécifiant deux "
"lignes se croisant dans une ligne est '<emphasis role=\"bold\">1*1***1**</"
"emphasis>' :"

#. Tag: para
#: using_postgis_query.xml:429
#, no-c-format
msgid ""
"For the second example, the intersection matrix pattern specifying a line "
"partly inside and partly outside a polygon is '<emphasis "
"role=\"bold\">102101FF2</emphasis>':"
msgstr ""
"Pour le deuxième exemple, le modèle de matrice d'intersection spécifiant une "
"ligne située en partie à l'intérieur et en partie à l'extérieur d'un "
"polygone est \"<emphasis role=\"bold\">102101FF2</emphasis>\" :"

#. Tag: title
#: using_postgis_query.xml:439
#, no-c-format
msgid "Using Spatial Indexes"
msgstr "Utilisation des index spatiaux"

#. Tag: para
#: using_postgis_query.xml:441
#, no-c-format
msgid ""
"When constructing queries using spatial conditions, for best performance it "
"is important to ensure that a spatial index is used, if one exists (see "
"<xref linkend=\"build-indexes\"/>). To do this, a spatial operator or index-"
"aware function must be used in a <code>WHERE</code> or <code>ON</code> "
"clause of the query."
msgstr ""
"Lors de la construction de requêtes utilisant des conditions spatiales, il "
"est important, pour de meilleures performances, de s'assurer qu'un index "
"spatial est utilisé, s'il existe (voir <xref linkend=\"build-indexes\"/>). "
"Pour ce faire, un opérateur spatial ou une fonction sensible à l'index doit "
"être utilisé dans une clause <code>WHERE</code> ou <code>ON</code> de la "
"requête."

#. Tag: para
#: using_postgis_query.xml:447
#, no-c-format
msgid ""
"Spatial operators include the bounding box operators (of which the most "
"commonly used is <xref linkend=\"geometry_overlaps\"/>; see <xref "
"linkend=\"operators-bbox\"/> for the full list) and the distance operators "
"used in nearest-neighbor queries (the most common being <xref "
"linkend=\"geometry_distance_knn\"/>; see <xref linkend=\"operators-"
"distance\"/> for the full list.)"
msgstr ""
"Les opérateurs spatiaux comprennent les opérateurs de boîte de délimitation "
"(dont le plus couramment utilisé est <xref linkend=\"geometry_overlaps\"/> ; "
"voir <xref linkend=\"operators-bbox\"/> pour la liste complète) et les "
"opérateurs de distance utilisés dans les requêtes sur les plus proches "
"voisins (le plus courant étant <xref linkend=\"geometry_distance_knn\"/> ; "
"voir <xref linkend=\"operators-distance\"/> pour la liste complète)."

#. Tag: para
#: using_postgis_query.xml:455
#, no-c-format
msgid ""
"Index-aware functions automatically add a bounding box operator to the "
"spatial condition. Index-aware functions include the named spatial "
"relationship predicates <xref linkend=\"ST_Contains\"/>, <xref "
"linkend=\"ST_ContainsProperly\"/>, <xref linkend=\"ST_CoveredBy\"/>, <xref "
"linkend=\"ST_Covers\"/>, <xref linkend=\"ST_Crosses\"/>, <xref "
"linkend=\"ST_Intersects\"/>, <xref linkend=\"ST_Overlaps\"/>, <xref "
"linkend=\"ST_Touches\"/>, <xref linkend=\"ST_Within\"/>, <xref "
"linkend=\"ST_Within\"/>, and <xref linkend=\"ST_3DIntersects\"/>, and the "
"distance predicates <xref linkend=\"ST_DWithin\"/>, <xref "
"linkend=\"ST_DFullyWithin\"/>, <xref linkend=\"ST_3DDFullyWithin\"/>, and "
"<xref linkend=\"ST_3DDWithin\"/> .)"
msgstr ""
"Les fonctions tenant compte de l'index ajoutent automatiquement un opérateur "
"de boîte de délimitation à la condition spatiale. Les fonctions tenant "
"compte de l'index comprennent les prédicats de relation spatiale nommés "
"<xref linkend=\"ST_Contains\"/>, <xref linkend=\"ST_ContainsProperly\"/>, "
"<xref linkend=\"ST_CoveredBy\"/>, <xref linkend=\"ST_Covers\"/>, <xref "
"linkend=\"ST_Crosses\"/>, <xref linkend=\"ST_Intersects\"/>, <xref "
"linkend=\"ST_Overlaps\"/>, <xref linkend=\"ST_Touches\"/>, <xref "
"linkend=\"ST_Within\"/>, <xref linkend=\"ST_Within\"/>, et <xref "
"linkend=\"ST_3DIntersects\"/>, et les prédicats de distance <xref "
"linkend=\"ST_DWithin\"/>, <xref linkend=\"ST_DFullyWithin\"/>, <xref "
"linkend=\"ST_3DDFullyWithin\"/>, et <xref linkend=\"ST_3DDWithin\"/> . )"

#. Tag: para
#: using_postgis_query.xml:476
#, no-c-format
msgid ""
"Functions such as <xref linkend=\"ST_Distance\"/> do <emphasis>not</"
"emphasis> use indexes to optimize their operation. For example, the "
"following query would be quite slow on a large table:"
msgstr ""
"Les fonctions telles que <xref linkend=\"ST_Distance\"/> n'utilisent "
"<emphasis>pas</emphasis> les index pour optimiser leur fonctionnement. Par "
"exemple, la requête suivante serait assez lente sur une grande table :"

#. Tag: para
#: using_postgis_query.xml:483
#, no-c-format
msgid ""
"This query selects all the geometries in <code>geom_table</code> which are "
"within 100 units of the point (100000, 200000). It will be slow because it "
"is calculating the distance between each point in the table and the "
"specified point, ie. one <varname>ST_Distance()</varname> calculation is "
"computed for <emphasis role=\"bold\">every</emphasis> row in the table."
msgstr ""
"Cette requête sélectionne toutes les géométries de la <code>geom_table</"
"code> qui se trouvent à moins de 100 unités du point (100000, 200000). Elle "
"sera lente car elle calcule la distance entre chaque point du tableau et le "
"point spécifié, c'est-à-dire qu'un calcul <varname>ST_Distance()</varname> "
"est effectué pour <emphasis role=\"bold\">chaque</emphasis> ligne du tableau."

#. Tag: para
#: using_postgis_query.xml:489
#, no-c-format
msgid ""
"The number of rows processed can be reduced substantially by using the index-"
"aware function <xref linkend=\"ST_DWithin\"/>:"
msgstr ""
"Le nombre de lignes traitées peut être considérablement réduit en utilisant "
"la fonction d'indexation <xref linkend=\"ST_DWithin\"/> :"

#. Tag: para
#: using_postgis_query.xml:495
#, no-c-format
msgid ""
"This query selects the same geometries, but it does it in a more efficient "
"way. This is enabled by <varname>ST_DWithin()</varname> using the "
"<varname>&amp;&amp;</varname> operator internally on an expanded bounding "
"box of the query geometry. If there is a spatial index on <code>geom</code>, "
"the query planner will recognize that it can use the index to reduce the "
"number of rows scanned before calculating the distance. The spatial index "
"allows retrieving only records with geometries whose bounding boxes overlap "
"the expanded extent and hence which <emphasis>might</emphasis> be within the "
"required distance. The actual distance is then computed to confirm whether "
"to include the record in the result set."
msgstr ""
"Cette requête sélectionne les mêmes géométries, mais de manière plus "
"efficace. Ceci est possible en <varname>ST_DWithin()</varname> utilisant "
"l'opérateur <varname>&amp;&amp;</varname> en interne sur une boîte de "
"délimitation élargie de la géométrie de la requête. S'il existe un index "
"spatial sur <code>geom</code>, le planificateur de requêtes reconnaîtra "
"qu'il peut utiliser l'index pour réduire le nombre de lignes analysées avant "
"de calculer la distance. L'index spatial permet d'extraire uniquement les "
"enregistrements dont les boîtes de délimitation chevauchent l'étendue "
"élargie et qui, par conséquent, <emphasis>pourraient</emphasis> se trouver à "
"l'intérieur de la distance requise. La distance réelle est ensuite calculée "
"pour confirmer l'inclusion de l'enregistrement dans l'ensemble des résultats."

#. Tag: para
#: using_postgis_query.xml:509
#, no-c-format
msgid ""
"For more information and examples see the <ulink url=\"https://postgis.net/"
"workshops/postgis-intro/indexing.html\">PostGIS Workshop</ulink>."
msgstr ""
"Pour plus d'informations et d'exemples, voir l' <ulink url=\"https://postgis."
"net/workshops/postgis-intro/indexing.html\">atelier de travail PostGIS</"
"ulink>."

#. Tag: title
#: using_postgis_query.xml:516
#, no-c-format
msgid "Examples of Spatial SQL"
msgstr "Exemples de SQL spatial"

#. Tag: para
#: using_postgis_query.xml:518
#, no-c-format
msgid ""
"The examples in this section make use of a table of linear roads, and a "
"table of polygonal municipality boundaries. The definition of the "
"<varname>bc_roads</varname> table is:"
msgstr ""
"Les exemples de cette section utilisent une table de routes linéaires et une "
"table de limites de communes polygonales. La définition de la table "
"<varname>bc_roads</varname> est la suivante :"

#. Tag: para
#: using_postgis_query.xml:524
#, no-c-format
msgid "The definition of the <varname>bc_municipality</varname> table is:"
msgstr ""
"La définition de la table <varname>bc_municipality</varname> est la "
"suivante :"

#. Tag: para
#: using_postgis_query.xml:532
#, no-c-format
msgid "What is the total length of all roads, expressed in kilometers?"
msgstr ""
"Quelle est la longueur totale de toutes les routes, exprimée en kilomètres ?"

#. Tag: para
#: using_postgis_query.xml:537
#, no-c-format
msgid "You can answer this question with a very simple piece of SQL:"
msgstr ""
"Vous pouvez répondre à cette question à l'aide d'un code SQL très simple :"

#. Tag: para
#: using_postgis_query.xml:546
#, no-c-format
msgid "How large is the city of Prince George, in hectares?"
msgstr "Quelle est la superficie de la ville de Prince George, en hectares ?"

#. Tag: para
#: using_postgis_query.xml:550
#, no-c-format
msgid ""
"This query combines an attribute condition (on the municipality name) with a "
"spatial calculation (of the polygon area):"
msgstr ""
"Cette requête combine une condition d'attribut (sur le nom de la "
"municipalité) avec un calcul spatial (de la surface du polygone) :"

#. Tag: para
#: using_postgis_query.xml:560
#, no-c-format
msgid "What is the largest municipality in the province, by area?"
msgstr ""
"Quelle est la plus grande municipalité de la province, en termes de "
"superficie ?"

#. Tag: para
#: using_postgis_query.xml:565
#, no-c-format
msgid ""
"This query uses a spatial measurement as an ordering value. There are "
"several ways of approaching this problem, but the most efficient is below:"
msgstr ""
"Cette requête utilise une mesure spatiale comme valeur de référence. Il "
"existe plusieurs façons d'aborder ce problème, mais la plus efficace est la "
"suivante :"

#. Tag: para
#: using_postgis_query.xml:571
#, no-c-format
msgid ""
"Note that in order to answer this query we have to calculate the area of "
"every polygon. If we were doing this a lot it would make sense to add an "
"area column to the table that could be indexed for performance. By ordering "
"the results in a descending direction, and them using the PostgreSQL "
"\"LIMIT\" command we can easily select just the largest value without using "
"an aggregate function like MAX()."
msgstr ""
"Notez que pour répondre à cette question, nous devons calculer la surface de "
"chaque polygone. Si nous faisions cela souvent, il serait logique d'ajouter "
"une colonne de surface à la table qui pourrait être indexée pour des raisons "
"de performance. En ordonnant les résultats dans une direction décroissante "
"et en utilisant la commande \"LIMIT\" de PostgreSQL, nous pouvons facilement "
"sélectionner la plus grande valeur sans utiliser une fonction d'agrégation "
"comme MAX()."

#. Tag: para
#: using_postgis_query.xml:583
#, no-c-format
msgid "What is the length of roads fully contained within each municipality?"
msgstr ""
"Quelle est la longueur des routes entièrement comprises dans chaque "
"municipalité ?"

#. Tag: para
#: using_postgis_query.xml:588
#, no-c-format
msgid ""
"This is an example of a \"spatial join\", which brings together data from "
"two tables (with a join) using a spatial interaction (\"contained\") as the "
"join condition (rather than the usual relational approach of joining on a "
"common key):"
msgstr ""
"Il s'agit d'un exemple de \"jointure spatiale\", qui rassemble les données "
"de deux tables (avec une jointure) en utilisant une interaction spatiale "
"(\"contenu\") comme condition de jointure (plutôt que l'approche "
"relationnelle habituelle de jointure sur une clé commune) :"

#. Tag: para
#: using_postgis_query.xml:596
#, no-c-format
msgid ""
"This query takes a while, because every road in the table is summarized into "
"the final result (about 250K roads for the example table). For smaller "
"datsets (several thousand records on several hundred) the response can be "
"very fast."
msgstr ""
"Cette requête prend un certain temps, car chaque route du tableau est "
"résumée dans le résultat final (environ 250 000 routes pour le tableau de "
"l'exemple). Pour des ensembles de données plus petits (plusieurs milliers "
"d'enregistrements sur plusieurs centaines), la réponse peut être très rapide."

#. Tag: para
#: using_postgis_query.xml:605
#, no-c-format
msgid "Create a new table with all the roads within the city of Prince George."
msgstr ""
"Créez un nouveau tableau avec toutes les routes de la ville de Prince George."

#. Tag: para
#: using_postgis_query.xml:610
#, no-c-format
msgid ""
"This is an example of an \"overlay\", which takes in two tables and outputs "
"a new table that consists of spatially clipped or cut resultants. Unlike the "
"\"spatial join\" demonstrated above, this query creates new geometries. An "
"overlay is like a turbo-charged spatial join, and is useful for more exact "
"analysis work:"
msgstr ""
"Il s'agit d'un exemple de \"superposition\", qui prend en compte deux "
"tableaux et produit un nouveau tableau composé de résultats coupés dans "
"l'espace. Contrairement à la \"jointure spatiale\" présentée ci-dessus, "
"cette requête crée de nouvelles géométries. Une superposition est comme une "
"jointure spatiale turbocompressée, et est utile pour des travaux d'analyse "
"plus précis :"

#. Tag: para
#: using_postgis_query.xml:623
#, no-c-format
msgid "What is the length in kilometers of \"Douglas St\" in Victoria?"
msgstr "Quelle est la longueur en kilomètres de \"Douglas St\" à Victoria ?"

#. Tag: para
#: using_postgis_query.xml:634
#, no-c-format
msgid "What is the largest municipality polygon that has a hole?"
msgstr "Quel est le plus grand polygone municipal comportant un trou ?"
