# generated automatically by aclocal 1.16.3 -*- Autoconf -*-

# Copyright (C) 1996-2020 Free Software Foundation, Inc.

# This file is free software; the Free Software Foundation
# gives unlimited permission to copy and/or distribute it,
# with or without modifications, as long as this notice is preserved.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY, to the extent permitted by law; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.

m4_ifndef([AC_CONFIG_MACRO_DIRS], [m4_defun([_AM_CONFIG_MACRO_DIRS], [])m4_defun([AC_CONFIG_MACRO_DIRS], [_AM_CONFIG_MACRO_DIRS($@)])])
m4_include([macros/ac_proj4_version.m4])
m4_include([macros/ac_protobufc_version.m4])
m4_include([macros/ax_cxx_compile_stdcxx.m4])
m4_include([macros/compiler-vendor.m4])
m4_include([macros/gettext.m4])
m4_include([macros/gtk-2.0.m4])
m4_include([macros/host-cpu-c-abi.m4])
m4_include([macros/iconv.m4])
m4_include([macros/intlmacosx.m4])
m4_include([macros/lib-ld.m4])
m4_include([macros/lib-link.m4])
m4_include([macros/lib-prefix.m4])
m4_include([macros/libtool.m4])
m4_include([macros/ltoptions.m4])
m4_include([macros/ltsugar.m4])
m4_include([macros/ltversion.m4])
m4_include([macros/lt~obsolete.m4])
m4_include([macros/nls.m4])
m4_include([macros/pkg.m4])
m4_include([macros/po.m4])
m4_include([macros/progtest.m4])
