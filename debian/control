Source: postgis
Maintainer: Debian GIS Project <pkg-grass-devel@lists.alioth.debian.org>
Uploaders: Stephen Frost <sfrost@debian.org>,
           Francesco Paolo Lovergine <frankie@debian.org>,
           Markus Wanner <markus@bluegap.ch>,
           Bas Couwenberg <sebastic@debian.org>,
           Christoph Berg <myon@debian.org>
Section: misc
Priority: optional
Build-Depends: bison,
               dctrl-tools,
               dblatex,
               debhelper-compat (= 13),
               docbook,
               docbook-xsl,
               dpkg-dev (>= 1.16.1~),
               flex,
               graphicsmagick-imagemagick-compat,
               libcunit1-dev,
               libgdal-dev (>= 1.11.2+dfsg-3~) | libgdal1-dev (>= 1.9.0~),
               libgeos-dev (>= 3.6),
               libjson-c-dev | libjson0-dev (>= 0.9~),
               libpcre2-dev,
               libproj-dev (>= 5.2.0),
               libprotobuf-c-dev,
               libsfcgal-dev (>= 1.3.1) [!armel !armhf],
               libxml2-dev (>= 2.5.0~),
               libxml2-utils,
               lsb-release,
               pkgconf,
               po-debconf,
               postgresql-all,
               postgresql-common (>= 148~),
               postgresql-server-dev-all,
               protobuf-c-compiler,
               rdfind,
               xsltproc
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/debian-gis-team/postgis
Vcs-Git: https://salsa.debian.org/debian-gis-team/postgis.git
Homepage: https://postgis.net/
Rules-Requires-Root: no

Package: postgis
Architecture: any
Depends: ${shlibs:Depends},
         ${perl:Depends},
         ${misc:Depends}
Recommends: postgresql-postgis,
            postgis-doc
Description: Geographic objects support for PostgreSQL
 PostGIS adds support for geographic objects to the PostgreSQL
 object-relational database. In effect, PostGIS "spatially enables"
 the PostgreSQL server, allowing it to be used as a backend spatial
 database for geographic information systems (GIS), much like ESRI's
 SDE or Oracle's Spatial extension. PostGIS follows the OpenGIS
 "Simple Features Specification for SQL".
 .
 This package contains the PostGIS userland binaries for importing and
 exporting shape and raster files: pgsql2shp, raster2pgsql, and shp2pgsql.

Package: postgis-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${misc:Depends}
Suggests: postgis
Description: Geographic objects support for PostgreSQL -- documentation
 PostGIS adds support for geographic objects to the PostgreSQL
 object-relational database. In effect, PostGIS "spatially enables"
 the PostgreSQL server, allowing it to be used as a backend spatial
 database for geographic information systems (GIS), much like ESRI's
 SDE or Oracle's Spatial extension. PostGIS follows the OpenGIS
 "Simple Features Specification for SQL".
 .
 This package contains the PostGIS documentation.

Package: postgresql-16-postgis-3
Architecture: any
Depends: postgresql-16,
         postgresql-16-postgis-3-scripts,
         ${shlibs:Depends},
         ${misc:Depends}
Suggests: postgis
Provides: postgresql-16-postgis
Description: Geographic objects support for PostgreSQL 16
 PostGIS adds support for geographic objects to the PostgreSQL
 object-relational database. In effect, PostGIS "spatially enables"
 the PostgreSQL server, allowing it to be used as a backend spatial
 database for geographic information systems (GIS), much like ESRI's
 SDE or Oracle's Spatial extension. PostGIS follows the OpenGIS
 "Simple Features Specification for SQL".

Package: postgresql-16-postgis-3-scripts
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Recommends: postgresql-16-postgis-3
Provides: postgresql-16-postgis-scripts
Description: Geographic objects support for PostgreSQL 16 -- SQL scripts
 PostGIS adds support for geographic objects to the PostgreSQL
 object-relational database. In effect, PostGIS "spatially enables"
 the PostgreSQL server, allowing it to be used as a backend spatial
 database for geographic information systems (GIS), much like ESRI's
 SDE or Oracle's Spatial extension. PostGIS follows the OpenGIS
 "Simple Features Specification for SQL".
 .
 This package contains the SQL scripts for installing PostGIS in a PostgreSQL
 16 database, and for upgrading from earlier PostGIS versions.

Package: postgresql-postgis
Architecture: any
Section: metapackages
Depends: postgresql-postgis-scripts,
         postgresql-16-postgis-3,
         ${misc:Depends}
Description: Geographic objects support for PostgreSQL -- Metapackage
 PostGIS adds support for geographic objects to the PostgreSQL
 object-relational database. In effect, PostGIS "spatially enables"
 the PostgreSQL server, allowing it to be used as a backend spatial
 database for geographic information systems (GIS), much like ESRI's
 SDE or Oracle's Spatial extension. PostGIS follows the OpenGIS
 "Simple Features Specification for SQL".
 .
 This metapackage depends on the PostGIS package for PostgreSQL 16.

Package: postgresql-postgis-scripts
Architecture: all
Multi-Arch: foreign
Depends: postgresql-16-postgis-3-scripts,
         ${misc:Depends}
Description: Geographic objects support for PostgreSQL -- SQL scripts metapackage
 PostGIS adds support for geographic objects to the PostgreSQL
 object-relational database. In effect, PostGIS "spatially enables"
 the PostgreSQL server, allowing it to be used as a backend spatial
 database for geographic information systems (GIS), much like ESRI's
 SDE or Oracle's Spatial extension. PostGIS follows the OpenGIS
 "Simple Features Specification for SQL".
 .
 This metapackage depends on the PostGIS scripts package for
 PostgreSQL 16.
